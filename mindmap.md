```mermaid
%%{init: { 'theme':'default' }}%%
mindmap
root (Improvisation)
    (Construire une histoire)
        💓 Ici et maintenant
            Utilisez les 5 sens odeur, toucher, ouïe, vue, gout
            Ne pas parler de quelquechose à l'extérieur
                En cas d'erreur, ramener à la relation entre les personnages
            Eviter effet de pont
                Ralentir la quête (souvent extérieure)
        Utiliser la moindre proposition
            Utiliser ce que l'autre a dit
            Cadeau pourri
            Si ça c'est vrai, quoi d'autre est vrai
        Transformation
            Il était une fois, ... changement à la fin
        Relation
            Quête intérieure
                S'intéresser à la relation
    (Acteur)
        Qualité
            Rester simple et évident
                Spontanéité
                    Recherche de l'instabilité
                    La partie drole arrive ici !
            Réagir instantanément
                Réagir rapidemment aux propositions
            Vivre le moment présent
            Rester connecté aux partenaires
        Défaut
            Vouloir être malin
            Vouloir être drôle
                Ejecte le public de la réalité de la scène
            Chercher la stabilité de la scène
    (Spectacle)
        Conseil
            Si à la 3ème scène tout est bon, il y a un problème
            Variez vos impros: rythme, personnage, relation, jeu
    (Formation)
        Elimier l'idée de compétition
        Affronter la peur 
            Célébrer l'échec
    (Que veut le public ?)
        Voir des gens qui prennent des risques
        Voir des gens qui échouent avec le sourire
            Transformation des erreurs en cadeaux
            Génère une expérience de vie et de mort cathartique
    (Principes)
        H2S: Healthy, Happy, Sexy
            Sourire face au danger
                S'autoriser des moments de vulnérabilité
        Fierté et ego
            Mettre son partenaire en valeur
                Est-ce qu'on a envie de jouer avec moi ?
            Il faut savoir mettre le frein à main ou venir à la rescousse
        Garder sa proposition, fusionner et explorer la Relation
        Etre compétent
            Entrer sur scène avec courage et aplomb
        Accepter
            Oui et , ou non qui amène du jeu
        Etre généreux
            Etre joueur
                Essayer de foutre l'autre dans la merde
        Vivre la scène
        
```
